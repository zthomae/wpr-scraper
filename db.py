#!/usr/bin/env python
import sqlite3

# Add a list of data items to the database
def add_to_db(data, conn):
    c = conn.cursor()
    try:
        data_tuples = [(d['date'].isoformat(), d['hour'], d['composer'], d['piece'],
                        d['extra'], d['length'], d['label'], d['format'])
                        for d in data]
        # Some pages have duplicate entries. Do naive deduplication.
        data_tuples = tuple(set(data_tuples))
        c.executemany('INSERT INTO listings VALUES (?,?,?,?,?,?,?,?)', data_tuples)
        # No troubles? Return True
        return True
    except sqlite3.Error, e:
        print e
        return False
    finally:
        conn.commit()

def fetch_all(conn):
    c = conn.cursor()
    data = []
    for row in c.execute("SELECT * from listings"):
        # Easier to work with dictionaries
        data.append({'date': row[0], 'hour': row[1], 'composer': row[2],
                'piece': row[3], 'extra': row[4], 'length': row[5],
                'label': row[6], 'format': row[7]})
    return data

def prepare_db(options):
    # Open db connection and get cursor
    conn = sqlite3.connect('wpr.db')
    c = conn.cursor()

    # See if listings table exists. If not, create it.
    c.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='listings'")
    if c.fetchone() is None:
        # Create table
        c.execute("""CREATE TABLE listings
                (date text NOT NULL, hour text NOT NULL, composer text NOT NULL,
                piece text NOT NULL, extra text, length int, label text, format text,
                PRIMARY KEY (date, hour, composer, piece, extra))""")
        conn.commit()
    # If there is one and you're rebuilding the history, delete all rows from the table.
    elif options.history:
        c.execute("DELETE FROM listings")
        conn.commit()

    # Done preparing. Return the connection.
    return conn

def find_last_date(conn):
    c = conn.cursor()
    return c.execute("SELECT max(date) from listings").fetchone()