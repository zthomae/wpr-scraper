#!/usr/bin/python
from db import fetch_all
import numpy as np
import matplotlib.pyplot as plt

def plot_composers(options, conn):
    data = fetch_all(conn)
    composers = {}
    for row in data:
        composer = row['composer']
        if composer not in composers:
            composers[composer] = 0.0
        composers[composer] += row['length']
    x = np.array(range(0, len(composers)))
    y = np.array(sorted(composers.values())[::-1])
    plt.figure('Composers')
    plt.plot(x, y, 'bo', markersize=3)
    plt.xlabel("nth composer")
    plt.ylabel("airtime (seconds)")
    plt.show()