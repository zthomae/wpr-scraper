#!/usr/bin/env python
import argparse
from datetime import datetime
from db import add_to_db, prepare_db, find_last_date
from plot import plot_composers
from scrape import scrape_archive_page, scrape_playlist_page
import sys

def parse_args():
    parser = argparse.ArgumentParser(
            description="Run the WPR scraper/data transmogrifier")
    parser.add_argument('--scrape', dest='scrape', action='store_const',
            const=True, default=False,
            help="Scrape the WPR site for new data")
    parser.add_argument('--build-history', dest='history', action='store_const',
            const=True, default=False,
            help="Rebuild the database from the full archives")
    parser.add_argument('--verbose', dest='verbose', action='store_const',
            const=True, default=False,
            help="Print verbose output")
    parser.add_argument('--plot-composers', dest='composers', action='store_const',
            const=True, default=False,
            help="Draw a plot of the total playing times for each composer")
    return parser.parse_args()

def scrape_entries(entries, options, conn):
    for entry in entries:
        data = scrape_playlist_page(entry)
        if options.verbose:
            print data
        # If adding to the database doesn't work, PANIC PANIC PANIC
        db_success = add_to_db(data, conn)
        if not db_success:
            print "Error adding to database on page: %s!" % entry['url']
            sys.exit(1)

def scrape_main(last_date, options, conn):
    entries = [e for e in scrape_archive_page() if e['date'] > last_date]
    scrape_entries(entries, options, conn)

def main():
    options = parse_args()
    conn = prepare_db(options)
    # If getting the whole history, set the last date far into
    # the past, so it gets all of the entries.
    if options.scrape or options.history:
        last_date = datetime.date(datetime.strptime('1901', '%Y'))
        if not options.history:
            try:
                last_date = datetime.date(datetime.strptime(find_last_date(conn)[0], '%Y-%m-%d'))
            except TypeError:
                pass
        scrape_main(last_date, options, conn)

    if options.composers:
        plot_composers(options, conn)

if __name__ == '__main__':
    main()