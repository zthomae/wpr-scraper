#!/usr/bin/env python
from bs4 import BeautifulSoup
from datetime import date, timedelta
import re
import requests
import sys
import time

# Get the total number of seconds in a timedelta in a Python 2.6-safe way
def total_seconds(td):
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6
    
# Generic function for scraping pages and running them through bs4
def make_soup(url):
    r = requests.get(url)
    # If there's any trouble, give up immediately
    if r.status_code != 200:
        print "Error downloading page: %s" % url
        return None
    return BeautifulSoup(r.content)

def url_to_date(url):
    date_pieces = map(int, url.split('?date=')[1].split('-'))
    return {'date': date(*date_pieces), 'url': url}

# Get a list of playlist pages to scrape
def scrape_archive_page():
    url_prefix = 'http://wpr.net'

    # Get playlist links
    soup = make_soup('http://wpr.net/guts/playlists/build_calendars_iframe.cfm')
    weeks = soup.find_all('tr', 'days')
    days_list= []
    for week in weeks:
        for a in week.find_all('a'):
            days_list.append(a['href'])

    # Turn data into a list of dictionaries with dates and pages
    days = map(url_to_date, days_list)

    # Sort by date
    days.sort(key=lambda x: x['date'])

    # For convenience, prepend url_prefix to each url
    for day in days:
        day['url'] = url_prefix + day['url']

    # Return the list of dictionaries
    return days

# Construct a list of objects representing playlist items.
# Note that we don't add anything to the database here -- that comes later. 
def scrape_playlist_page(page_meta):
    soup = make_soup(page_meta['url'])

    # Get playlist entries
    rows = soup.find_all('tr', valign='top')
    entries = [row for row in rows if row.find_all('td')[1].text]

    # Turn rows into data
    data = []
    hour = ''
    for entry in entries:
        tds = entry.find_all('td')

        # Not every row has an hour in it. Update the hour when there is.
        hour_maybe = tds[0].text.strip()
        if hour_maybe:
            hour = hour_maybe

        # At least this is easy
        composer = tds[1].text.strip()

        # Grab the piece name and other info (conductor, etc)
        selection = re.sub('<br/?>', '\n', str(tds[2]))
        selection_text = BeautifulSoup(selection).text.strip()

        if '\n' in selection_text:
            piece, extra = selection_text.split('\n', 1)
        else:
            piece = selection_text
            extra = None

        # Length of piece and label information combined in one td.
        label_length = tds[3].text.strip()
        if label_length != '':
            # Try to get length
            label_length_split = label_length.rsplit(u'\xa0', 1)
            # Sometimes there is no listen length
            if len(label_length_split) == 2:
                label_string, length_string = [l.strip() for l in label_length_split]
            else:
                label_string = label_length_split[0]
                length_string = ''
            if length_string != '':
                num_minutes, num_seconds = map(int, length_string.split(':'))
                if num_minutes < 60:
                    num_hours = 0
                else:
                    num_hours = num_minutes / 60
                    num_minutes %= 60
                length_string = '%d:%d:%d' % (num_hours, num_minutes, num_seconds)
                length_time = time.strptime(length_string, '%H:%M:%S')
                td = timedelta(hours=length_time.tm_hour, minutes=length_time.tm_min,
                        seconds=length_time.tm_sec)
                length = total_seconds(td)
            else:
                length = total_seconds(timedelta(0))
                
            # Get the label and format
            if '\n' in label_string:
                label, format = label_string.split('\n', 1)
            else:
                label = label_string
                format = None

        # Add dictionary to data list
        data.append({'date': page_meta['date'], 'hour': hour, 'composer': composer,
                'piece': piece, 'extra': extra, 'length': length,
                'label': label, 'format': format})

    # Return the list, to be processed elsewhere
    return data